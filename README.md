# minimap2 Singularity container
### Bionformatics package minimap2<br>
Minimap2 is a versatile sequence alignment program that aligns DNA or mRNA sequences against a large reference database. Typical use cases include: (1) mapping PacBio or Oxford Nanopore genomic reads to the human genome; (2) finding overlaps between long reads with error rate up to ~15%; (3) splice-aware alignment of PacBio Iso-Seq or Nanopore cDNA or Direct RNA reads against a reference genome; (4) aligning Illumina single- or paired-end reads; (5) assembly-to-assembly alignment; (6) full-genome alignment between two closely related species with divergence below ~15%.<br>
minimap2 Version: 2.17<br>
[https://github.com/lh3/minimap2]

Singularity container based on the recipe: Singularity.minimap2_v2.17

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build minimap2_v2.17.sif Singularity.minimap2_v2.17`

### Get image help
`singularity run-help ./minimap2_v2.17.sif`

#### Default runscript: STAR
#### Usage:
  `minimap2_v2.17.sif --help`<br>
    or:<br>
  `singularity exec minimap2_v2.17.sif minimap2 --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull minimap2_v2.17.sif oras://registry.forgemia.inra.fr/gafl/singularity/minimap2/minimap2:latest`


